package cloning.model;

public class LineSegment implements Cloneable{
    private Point startPoint;
    private Point endPoint;

    public LineSegment(Point startPoint, Point endPoint) {
        this.startPoint = startPoint;
        this.endPoint = endPoint;
    }

    public Point getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(Point startPoint) {
        this.startPoint = startPoint;
    }

    public Point getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(Point endPoint) {
        this.endPoint = endPoint;
    }

    public Object clone() throws CloneNotSupportedException {
        Point newStartPoint = (Point) this.getStartPoint().clone();
        Point newEndPoint = (Point) this.getEndPoint().clone();
        return new LineSegment(newStartPoint, newEndPoint);
    }

    @Override
    public String toString() {
        return "LineSegment{" +
                "startPoint=" + startPoint +
                ", endPoint=" + endPoint +
                '}';
    }
}
