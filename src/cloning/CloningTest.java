package cloning;

import cloning.model.LineSegment;
import cloning.model.Point;

import java.util.ArrayList;
import java.util.Arrays;

public class CloningTest {
    public static void main (String args[]) throws CloneNotSupportedException {
        Point point1 = new Point(10,10);
        Point point2 = (Point) point1.clone();
        Point point3 = new Point(10,10);
        Point point4 = new Point(0,0);

        ArrayList<Point> points = new ArrayList<>(Arrays.asList(point1, point2, point3));
        points.forEach(System.out::println);

        System.out.println("point1 and point2 are the same: " + (point1 == point2));
        System.out.println("point1 and point3 are the same: " + (point1 == point3));

        System.out.println();
        System.out.println("Setting new coordinates for points");
        point2.setX(11);
        point3.setX(12);
        points.forEach(System.out::println);
        System.out.println();

        System.out.println("creating lines");
        LineSegment lineSegment1 = new LineSegment(point4, point1);
        LineSegment lineSegment2 = (LineSegment) lineSegment1.clone();
        System.out.println(lineSegment1);
        System.out.println(lineSegment2);

        System.out.println("lineSegment1 and lineSegment2 are the same: " + (lineSegment1 == lineSegment2));

        System.out.println("creating array and cloning it");
        int[][] x = {{1, 2}, {3, 4}};
        System.out.println(Arrays.deepToString(x));
        int[][] y = x.clone();
        System.out.println("arrays after cloning:");
        System.out.println(Arrays.deepToString(x));
        System.out.println(Arrays.deepToString(y));
    }
}
